# Lab Stacks

## Introduction

This repository contains dockerfiles which are used to build a variety of services commonly required in network lab setups.

These stacks can be deployed in two ways:

1) Standalone, installed on a linux host which directly hosts the containers for the stack
2) Using docker-in-docker to provide a "Lab Stacks as a Service"

## Standalone Deployment

Local deployment is great for use cases where a linux machine dedicated to a project already exists, or something is needed longer-term. This is oftem the easiest method to deploy.

1) Acquire a linux server (Debian or Ubuntu is recommended, but it shouldn't make much difference)
2) Install docker and docker-compose. Doing this is beyond the scope of this document, google is your friend!
3) In a directory of your choosing, make a new directory and go into it: `mkdir lab_stacks && cd lab_stacks`
4) Download the docker-compose.yml file which defines these services: `wget https://gitlab.com/dchidell/lab-stacks/-/raw/main/infra_dind_manager/docker-compose.yml`
6) Pull the containers (required, otherwise compose will try and build them): `docker-compose pull`
7) Bring up the service stack: `docker-compose up -d --no-build`
8) That's it! Browsing to the machine's IP via HTTP on port 80 should bring up the web interface.

### Potential issues:
* Ensure the machine has unhindered internet connectivity, internet connectivity is required to download the compose file and the containers
* You get an error starting the services. This may be because you already have other services using the ports in question. Depending on your use-case you may need to commend out services which can be found in the `docker-compose.yml` file (DNS is a common service to cause problems here). Alternatively you may need to stop the local conflicting services on the machine before these can be brought up.
